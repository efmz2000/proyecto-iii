int entrada=0;
char leer;

const long A = 1000;       //Resistencia en oscuridad en KΩ
const int B = 15;         //Resistencia a la luz (10 Lux) en KΩ
const int Rc = 10;       //Resistencia calibracion en KΩ
const int LDRPin2 = A1; //Pin del LDR
const int LDRPin = A0; //Pin del LDR
//variables para la medicion de la luz
int V;
int V2;
int ilum;
int ilum2;

const int Bateria= A2; // pin para medir la cantidad de bateria restante

const int Motor1= 5;     //pin 1 de motor delantero
const int Motor2= 6;    //pin 2 de motor delantero
const int Motor3= 10;  //pin 1 de motor trasero
const int Motor4= 11; //pin 2 de motor trasero

const int blanca_izq=13;
const int blanca_der=12;
const int naranja_f_d=9;
const int naranja_f_i=8;
const int naranja_a_d=7;
const int naranja_a_i=4;
int spd=0;//variable para la velocidad
int luces=LOW;//variable pra el esatdo de las luces delanteras
int DirL=LOW;//variable para saber el estado de las direccionales izquierdas
int DirR=LOW;//variable para saber el estado de las direccionales derechas
void setup() {
  Serial.begin(115200);
  pinMode(Motor1,OUTPUT);
  pinMode(Motor2,OUTPUT);
  pinMode(Motor3,OUTPUT);
  pinMode(Motor4,OUTPUT);
  pinMode(Bateria,OUTPUT);

  pinMode(blanca_izq,OUTPUT);        //definir pin como salida(blanco-frontal derecho)
  pinMode(blanca_der,OUTPUT);       //definir pin como salida(blanco-frontal izquierdo)
  pinMode(naranja_f_d, OUTPUT);    //definir pin como salida(naranja-frontal derecha)
  pinMode(naranja_f_i , OUTPUT);  //definir pin como salida(naranja-frontal izquierda)
  pinMode(naranja_a_d , OUTPUT); //definir pin como salida(naranja-trasera derecha)
  pinMode(naranja_a_i , OUTPUT);//definir pin como salida (naranja-trasera izquierda)
}

void loop() {
  // calculos de las fotoresistencias
   V = analogRead(LDRPin);         
   V2= analogRead(LDRPin2);         
   ilum = ((long)V*A*10)/((long)B*Rc*(1024-V));  //convierte la lectura de las fotoresistencias a un dato entre 0y 250  
   ilum2= ((long)V2*A*10)/((long)B*Rc*(1024-V2));

   ///fotoresit 1
   if (ilum<100){//si la ilumacion es <100 prende la luz
    
    digitalWrite(13,HIGH);
    }
   if (ilum>100){//si la ilumacion es >100 apaga la luz
   
   digitalWrite(13,LOW);
   }

   // foto resist 2
   if (ilum2<100){//si la ilumacion es <100 prende la luz
    digitalWrite(12,HIGH);
    
    }
   if (ilum2>100){//si la ilumacion es >100 apaga la luz
   digitalWrite(12,LOW);
 
   }
  //programa general segun instrucciones 
  entrada=Serial.read();
  
  digitalWrite(blanca_der,luces);
  while(DirR==HIGH){
    digitalWrite(naranja_f_d,(HIGH));
    digitalWrite(naranja_a_d,(HIGH));
    delay(1000);
    digitalWrite(naranja_f_d,(LOW));
    digitalWrite(naranja_a_d,(LOW));
  }
  while(DirR==HIGH){
    digitalWrite(naranja_f_i,(HIGH));
    digitalWrite(naranja_a_i,(HIGH));
    delay(1000);
    digitalWrite(naranja_f_i,(LOW));
    digitalWrite(naranja_a_i,(LOW));
  }
  if ( entrada==("spd:1")){
    if (spd<3){
      spd=spd+1;
      }
    }
  if ( entrada==("Spd:-1")){
    if (spd>-3){
      spd=spd-1;
    }
    
  }
  if ( entrada==("dir:1")){
    analogWrite(Motor1,255);
    analogWrite(Motor2,0);
    delay(500);
    analogWrite(Motor1,0);
    analogWrite(Motor2,0);
  }
  if ( entrada==("dir:-1")){
    analogWrite(Motor1,0);
    analogWrite(Motor2,255);
    delay(500);
    analogWrite(Motor1,0);
    analogWrite(Motor2,0);
    
  }
  if ( entrada==("Circle:-1")){
    analogWrite(Motor1,0);
    analogWrite(Motor2,255);
    delay(500);
    analogWrite(Motor1,0);
    analogWrite(Motor2,0);
    analogWrite(Motor3,255);
    analogWrite(Motor4,0);
    delay(2000);
    analogWrite(Motor3,0);
    analogWrite(Motor4,0);
    
  }
  if ( entrada==("Circle:1")){
    //gira a la der las llantas
    analogWrite(Motor1,255);
    analogWrite(Motor2,0);
    delay(500);
    //avanza para girrar
    analogWrite(Motor1,0);
    analogWrite(Motor2,0);
    analogWrite(Motor3,255);
    analogWrite(Motor4,0);
    delay(2000);
    analogWrite(Motor3,0);
    analogWrite(Motor4,0);
    
  }
  if ( entrada==("U-turn")){
    //gira a la der las llantas
    analogWrite(Motor1,255);
    analogWrite(Motor2,0);
    delay(500);
    //avanza para girrar 
    analogWrite(Motor1,0);
    analogWrite(Motor2,0);
    analogWrite(Motor3,255);
    analogWrite(Motor4,0);
    delay(1000);
    //detiene los motores
    analogWrite(Motor3,0);
    analogWrite(Motor4,0);
    analogWrite(Motor1,0);
    // endereza las llantas delanteras
    analogWrite(Motor2,255);
    delay(500);
    analogWrite(Motor1,0);
    analogWrite(Motor2,0);
    
    
  }
  if ( entrada==("Infinite")){
    //gira a la der las llantas
    analogWrite(Motor1,255);
    analogWrite(Motor2,0);
    delay(500);
    //avanza para girrar
    analogWrite(Motor1,0);
    analogWrite(Motor2,0);
    analogWrite(Motor3,255);
    analogWrite(Motor4,0);
    delay(1500);
    analogWrite(Motor3,0);
    analogWrite(Motor4,0);
    //Segunda parte del giro 
    analogWrite(Motor1,0);
    analogWrite(Motor2,255);
    delay(500);
    analogWrite(Motor1,0);
    analogWrite(Motor2,0);
    analogWrite(Motor3,255);
    analogWrite(Motor4,0);
    delay(1500);
    analogWrite(Motor3,0);
    analogWrite(Motor4,0);
  }
  if ( String(entrada)==("dirL")){
    if (DirL==LOW){
      DirL==HIGH;
    }
    else{
      DirL==LOW;
    }
    
  }
  if ( String(entrada)==("dirR")){
     if (DirR==LOW){
      DirR==HIGH;
    }
    else{
      DirR==LOW;
    }
    
  }
  if ( entrada==("hl")){
    if (luces==HIGH){
    luces=LOW;
    digitalWrite(blanca_der,luces);
    }
    else{
    luces=HIGH;
    digitalWrite(blanca_der,luces);
  }}
  if ( entrada==("stop")){
    spd=0;
        
  }
  if (entrada==("blvl")){
    Serial.write(Bateria);
  }
  if (entrada==('*')){
    Serial.write(ilum+ilum2/2);
  }
  if (spd==0){
    analogWrite(Motor3,0);
    analogWrite(Motor4,0);
  }
  if (spd==1){
    analogWrite(Motor3,150);
    analogWrite(Motor4,0);
  }
  if (spd==2){
    analogWrite(Motor3,200);
    analogWrite(Motor4,0);
  }
  if (spd==3){
    analogWrite(Motor3,255);
    analogWrite(Motor4,0);
  }
  if (spd==-1){
    analogWrite(Motor3,0);
    analogWrite(Motor4,150);
  }
  if (spd==-2){
    analogWrite(Motor3,0);
    analogWrite(Motor4,200);
  }
  if (spd==-3){
    analogWrite(Motor3,0);
    analogWrite(Motor4,255);
  }
 
}
