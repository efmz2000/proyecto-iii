'''
Tecnológico de Costa Rica
Carrera: Ingenieria en Computadores
Curso: Taller de Programación
Profesor: Milton Villegas Lemus
Autores:
Erick Fabian Madrigal 
Carne:2018146983
Hugo Carranza
Carne:2017167434
Version:1.5
Descripción:Programa que crea una interfaz grafica para
la comunicacion con el At-mega

'''

import pygame
from pygame.locals import *
from threading import Thread
from udpClient import *
import time

#Funcion que envia el mensaje para aumentar la velocidad
def avanzar():
        UdpMessage('spd:1')
        
#Funcion que envia el mensaje para disminuir la celocidad
def retroceder():
        UdpMessage('spd:-1')
        
#Funcion que envia el mensaje para mover la direccion a la derecha
def girar_der():
        UdpMessage('dir:1')
        
#Funcion que envia el mensaje para mover la direccion a la izquierda
def girar_izq():
        UdpMessage('dir:-1')

#Funcion que envia el mensaje para realizar el circulo a la izquierda 
def circulo_izq():
        UdpMessage('Circle:-1')

#Funcion que envia el mensaje para realizar el circulo a la derecha
def circulo_der():
        UdpMessage('Circle:1')

#Funcion que envia el mensaje para realizar la vuelta en U
def vuelta_u():
        UdpMessage('U-turn')

#Funcion que envia el mensaje para realzar el infinito
def ocho():
        UdpMessage('Infitite')

#Funcion que envia el mensaje para encender las direccionales izquierdas
def dirL():
        UdpMessage('dirL')

#Funcion que envia el mensaje para encender las direccionales derechas
def dirR():
        UdpMessage('dirR')

#Funcion que envia el mensaje para encender las luces delanteras
def headlights():
        UdpMessage('hl')

#Funcion que envia el mensaje para detener el auto 
def stop():
        UdpMessage('stop')

#Funcion que envia el mensaje para obtener el nivel de bateria
def bateria():
        bateria=UdpMessage('blvl')
        return str(bateria)
#Funcion que envia el mensaje para obtener la informacion de las fotoresistencias
def sensores():
        Sens_luz=UdpMessage('*')
        texto1=Sens_luz*100//255
        return str(texto1)

#Funcion para crear la ventana
def main():
        pygame.init()
        #colores 
        Blanco=(255,255,255)
        Negro=(0,0,0)
        Gris=(127,127,127)
        #carga de imagenes
        Adelante=pygame.image.load("Recursos/flecha_arb.png")
        Izquierda=pygame.image.load("Recursos/flecha_izq.png")
        Derecha =pygame.image.load("Recursos/flecha_der.png")
        Atras=pygame.image.load("Recursos/flecha_abj.png")
        Luz=pygame.image.load("Recursos/luz_blanca.jpg")
        Dirl=pygame.image.load("Recursos/dirl.jpg")
        Dirr=pygame.image.load("Recursos/dirr.jpg")
        Ocho=pygame.image.load("Recursos/ocho.png")
        Circ_i=pygame.image.load("Recursos/circulo_i.png")
        Circ_d=pygame.image.load("Recursos/circulo_d.png")
        Vuelta_U=pygame.image.load("Recursos/vuelta_u.png")
        Act_S=pygame.image.load("Recursos/actualizar_s.png")
        Act_V=pygame.image.load("Recursos/actualizar_v.png")
        Stop=pygame.image.load("Recursos/stop.png")

        #Texto
        Sens_luz="Cargando"
        Bateria=100
        fuente = pygame.font.Font(None, 40)
        texto1 = fuente.render(('la medidicion de la luz es: 0'), 0, Negro)
        texto2 = fuente.render(('Bateria restante: '+str(Bateria)+'%'), 0, Negro)

        #cargar imagenes como sprites

        #Boton para parar el auto
        Btn_stop=pygame.sprite.Sprite()
        Btn_stop.image=Stop
        Btn_stop.rect=Stop.get_rect()
        Btn_stop.rect.top=200
        Btn_stop.rect.left=250

        #Boton para encender las direccionales izquierdas
        Btn_DirL=pygame.sprite.Sprite()
        Btn_DirL.image=Dirl
        Btn_DirL.rect=Dirl.get_rect()
        Btn_DirL.rect.top=265
        Btn_DirL.rect.left=185

        #Boton para realizar el giro en u 
        Btn_u=pygame.sprite.Sprite()
        Btn_u.image=Vuelta_U
        Btn_u.rect=Vuelta_U.get_rect()
        Btn_u.rect.top=100
        Btn_u.rect.left=400

        #Boton para boton para hacer el circulo a la izquierda
        Btn_circL=pygame.sprite.Sprite()
        Btn_circL.image=Circ_i
        Btn_circL.rect=Circ_i.get_rect()
        Btn_circL.rect.top=265
        Btn_circL.rect.left=5

        #Boton para hacer el circulo a la derecha 
        Btn_circR=pygame.sprite.Sprite()
        Btn_circR.image=Circ_d
        Btn_circR.rect=Circ_d.get_rect()
        Btn_circR.rect.top=265
        Btn_circR.rect.left=380

        #Boton para realizar la figura del infinito
        Btn_8=pygame.sprite.Sprite()
        Btn_8.image=Ocho
        Btn_8.rect=Ocho.get_rect()
        Btn_8.rect.top=350
        Btn_8.rect.left=200

        #Boton para encender las direccionales izquierdas
        Btn_DirR=pygame.sprite.Sprite()
        Btn_DirR.image=Dirr
        Btn_DirR.rect=Dirr.get_rect()
        Btn_DirR.rect.top=265
        Btn_DirR.rect.left=315

        #Boton para prender las luces delanteras
        Btn_Luz=pygame.sprite.Sprite()
        Btn_Luz.image=Luz
        Btn_Luz.rect=Luz.get_rect()
        Btn_Luz.rect.top=265        
        Btn_Luz.rect.left=250

        #Boton para aumentar la velocidad del vehiculo 
        Btn_Adelante=pygame.sprite.Sprite()
        Btn_Adelante.image=Adelante
        Btn_Adelante.rect=Adelante.get_rect()
        Btn_Adelante.rect.top=135
        Btn_Adelante.rect.left=290

        #Boton para disminuir la velocidad del vehiculo  
        Btn_Atras=pygame.sprite.Sprite()
        Btn_Atras.image=Atras
        Btn_Atras.rect=Atras.get_rect()
        Btn_Atras.rect.top=140
        Btn_Atras.rect.left=215

        #Boton para girar la direccion a la izquierda
        Btn_Izquierda=pygame.sprite.Sprite()
        Btn_Izquierda.image=Izquierda
        Btn_Izquierda.rect=Izquierda.get_rect()
        Btn_Izquierda.rect.top=200
        Btn_Izquierda.rect.left=185

        #Boton para girar la direccion a la derercha 
        Btn_Derecha=pygame.sprite.Sprite()
        Btn_Derecha.image=Derecha
        Btn_Derecha.rect=Derecha.get_rect()
        Btn_Derecha.rect.top=199
        Btn_Derecha.rect.left=315

        #Boton para recibir los datos los sensores de luz
        Btn_ActS=pygame.sprite.Sprite()
        Btn_ActS.image=Act_S
        Btn_ActS.rect=Act_S.get_rect()
        Btn_ActS.rect.top=0
        Btn_ActS.rect.left=0

        #Boton para recibir la cantidad de bateria restante
        Btn_ActV=pygame.sprite.Sprite()
        Btn_ActV.image=Act_V
        Btn_ActV.rect=Act_V.get_rect()
        Btn_ActV.rect.top=66
        Btn_ActV.rect.left=0
        
        ventana=pygame.display.set_mode([600,480])#se crea la ventana 
        pygame.display.set_caption('Control del auto')#se le pone titulo a la ventana 
        salir=False#variable para establecer el cierre de la ventana 
        reloj=pygame.time.Clock()

        #rectangulo del mouse
        #Rectangulo del mouse
        r1=pygame.Rect(5,5,5,5)
        while salir != True:
            #parte de la funcion para cerrar la ventana 
            for event in pygame.event.get():
                    if event.type == pygame.KEYDOWN:
                            if event.key == pygame.K_F4:
                                    salir=True
                    if event.type== pygame.QUIT:
                            salir=True
                    

            reloj.tick(120)
            ventana.fill(Blanco)#colorea el fondo de la ventana

            #carga del texto
            ventana.blit(texto1,(75,10))
            ventana.blit(texto2,(75,90))
            #carga una imagen como un rectangulo para poder interactuar con ellas                
            ventana.blit(Btn_Izquierda.image,Btn_Izquierda.rect)
            ventana.blit(Btn_Atras.image,Btn_Atras.rect)
            ventana.blit(Btn_Derecha.image,Btn_Derecha.rect)
            ventana.blit(Btn_Adelante.image,Btn_Adelante.rect)
            ventana.blit(Btn_Luz.image,Btn_Luz.rect)
            ventana.blit(Btn_DirL.image,Btn_DirL.rect)
            ventana.blit(Btn_DirR.image,Btn_DirR.rect)
            ventana.blit(Btn_8.image,Btn_8.rect)
            ventana.blit(Btn_circL.image,Btn_circL.rect)
            ventana.blit(Btn_circR.image,Btn_circR.rect)
            ventana.blit(Btn_ActV.image,Btn_ActV.rect)
            ventana.blit(Btn_ActS.image,Btn_ActS.rect)
            ventana.blit(Btn_u.image,Btn_u.rect)
            ventana.blit(Btn_stop.image,Btn_stop.rect)
            
                
            
            
        
            #se le da una variable la posicion del mouse 
            #para poder detectar la colision
            (r1.left,r1.top)=pygame.mouse.get_pos()
            r1.left-=r1.width/2
            r1.top-=r1.height/2

            #en los siguientes if se detecta la colision del rectangulo del mouse (r1)
            #con los diferentes botones y al hacer click interactuan,
            #estos a su vez mediante hilos llaman a
            #la respectiva función
            if r1.colliderect(Btn_stop):
                    if event.type==pygame.MOUSEBUTTONDOWN:
                            C1=Thread(target=stop,args=())
                            C1.start()
                            time.sleep(1) 
            
            if r1.colliderect(Btn_Izquierda):
                    if event.type==pygame.MOUSEBUTTONDOWN:
                            C1=Thread(target=girar_izq,args=())
                            C1.start()
                            time.sleep(1)
                            
            if r1.colliderect(Btn_Atras):
                    if event.type==pygame.MOUSEBUTTONDOWN:
                            C1=Thread(target=retroceser,args=())
                            C1.start()
                            time.sleep(1)
                            
            if r1.colliderect(Btn_Derecha):
                    if event.type==pygame.MOUSEBUTTONDOWN:
                            C1=Thread(target=girar_der,args=())
                            C1.start()
                            time.sleep(1)
                            
            if r1.colliderect(Btn_Adelante):
                    if event.type==pygame.MOUSEBUTTONDOWN:
                            C1=Thread(target=avanzar,args=())
                            C1.start()
                            time.sleep(1)
                                                        
                            
            if r1.colliderect(Btn_Luz):
                    if event.type==pygame.MOUSEBUTTONDOWN:
                            C1=Thread(target=headlights,args=())
                            C1.start()
                            time.sleep(1)
                            
                            
                            
            if r1.colliderect(Btn_DirL):
                    if event.type==pygame.MOUSEBUTTONDOWN:
                            C1=Thread(target=dirL,args=())
                            C1.start()
                            time.sleep(1)
                            
            if r1.colliderect(Btn_DirR):
                    if event.type==pygame.MOUSEBUTTONDOWN:
                            C1=Thread(target=dirR,args=())
                            C1.start()
                            time.sleep(1)
                            
            if r1.colliderect(Btn_8):
                    if event.type==pygame.MOUSEBUTTONDOWN:
                            C1=Thread(target=ocho,args=())
                            C1.start()
                            time.sleep(1)
                            
            if r1.colliderect(Btn_circL):
                    if event.type==pygame.MOUSEBUTTONDOWN:
                            C1=Thread(target=circulo_izq,args=())
                            C1.start()
                            time.sleep(1)
                            
            if r1.colliderect(Btn_circR):
                    if event.type==pygame.MOUSEBUTTONDOWN:
                            C1=Thread(target=circulo_der,args=())
                            C1.start()
                            time.sleep(1)
                            
                            
            if r1.colliderect(Btn_u):
                    if event.type==pygame.MOUSEBUTTONDOWN:
                            C1=Thread(target=vuelta_u,args=())
                            C1.start()
                            time.sleep(1)
        
                       
            
            if r1.colliderect(Btn_ActV):
                    if event.type==pygame.MOUSEBUTTONDOWN:
                        texto2 = sensores()
                        texto2 = fuente.render(('Bateria restante: '+str(Bateria)+'%'), 0, Negro)
                        ventana.blit(texto2,(75,15))
                        time.sleep(1)
                        
            if r1.colliderect(Btn_ActS):
                    if event.type==pygame.MOUSEBUTTONDOWN:
                        Sens_luz = sensores()
                        texto1 = fuente.render(('La medidicion de la luz es: '+Sens_luz), 0, Negro)
                        ventana.blit(texto1,(75,15))
                        time.sleep(1)

            if r1.colliderect(Btn_Adelante):
                    if event.type==pygame.MOUSEBUTTONDOWN:
                        C1=Thread(target=avanzar,args=())
                        C1.start()      
                        time.sleep(1)
                        
            pygame.display.update()
            
        pygame.quit()
        
main()
